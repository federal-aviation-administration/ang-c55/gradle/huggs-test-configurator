# huggs-test-configurator

[![License: CC0-1.0](https://raster.shields.io/badge/License-CC0%201.0-lightgrey.png)](http://creativecommons.org/publicdomain/zero/1.0/)

Gradle Plugin to assist in providing opinionated/conventional configuration of test tasks

[[_TOC_]]


## Description

This plugin aims to simplify the configuration of test running in Gradle projects. It does this by automatically 
applying necessary plugins, dependencies, and task configurations.    


## Configuring the Plugin

Currently, this plugin does not allow for user configuration.


## Plugins Applied

* [java](https://docs.gradle.org/current/userguide/java_plugin.html) core plugin


## Configuration Applied

* Uses [JUnit 5.7.2](https://junit.org/junit5/docs/5.7.2/user-guide/index.html)
* Adds JUnit 5 Jupiter API library (`junit-jupiter-api`) to `testImplementation` 
  [dependency configuration](https://docs.gradle.org/current/userguide/java_plugin.html#tab:configurations)
* Adds JUnit 5 Jupiter engine library (`junit-jupiter-engine`) to `testRuntimeOnly`
  [dependency configuration](https://docs.gradle.org/current/userguide/java_plugin.html#tab:configurations)
* Configures all existing [Test](https://docs.gradle.org/current/dsl/org.gradle.api.tasks.testing.Test.html) tasks to 
  use JUnit 5
* Configures all existing [Test](https://docs.gradle.org/current/dsl/org.gradle.api.tasks.testing.Test.html) tasks to 
  show 
  standard streams to console during test execution


## Building the Plugin

Gradle is the build tool used for this project. The included wrapper can be used in lieu of installing Gradle. The only 
prerequisite is the JDK 8 runtime.

To build the software execute the follow:

```commandline
./gradlew build
```

This will build, test, and package the plugin.


## License

### Creative Commons Zero v1.0 Universal

This is a work created by or on behalf of the United States Government. To the extent that this work is not already in
the public domain by virtue of 17 USC § 105, the FAA waives copyright and neighboring rights in the work worldwide
through the CC0 1.0 Universal Public Domain Dedication (which can be found at https://creativecommons.org/publicdomain/zero/1.0/).

See [LICENSE.txt](LICENSE.txt) and [NOTICE.txt](NOTICE.txt) in the root of this project for the full terms of the license.


### Third Party

This project utilizes 3rd party libraries that are distributed under their own terms. For a complete list of libraries
and coordinating licenses, see [LICENSE-3RD-PARTY.txt](LICENSE-3RD-PARTY.txt) in the root of this project.
