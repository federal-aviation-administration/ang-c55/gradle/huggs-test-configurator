@file:Suppress("unused", "UnstableApiUsage")

import org.jetbrains.kotlin.gradle.tasks.KotlinCompile

plugins {
    id("com.gradle.plugin-publish") version "0.15.0"
    id("java-gradle-plugin")
    id("maven-publish")
    id("org.jetbrains.kotlin.jvm") version "1.3.72"
    id("org.unbroken-dome.test-sets") version "2.2.1"
    id("org.jetbrains.dokka") version "0.9.18"
}

apply(from = "third-party-license-report-config.gradle")
apply(from = "publication-license-config.gradle")

group = "us.gov.dot.faa.ang.c55"
version = (if(project.hasProperty("softwareVersion")) project.property("softwareVersion") else "")!!

repositories {
    mavenCentral()
}

dependencies {

    implementation(platform("org.jetbrains.kotlin:kotlin-bom"))
    implementation(group = "org.jetbrains.kotlin", name = "kotlin-stdlib-jdk8")
    implementation(group = "io.github.microutils", name = "kotlin-logging", version = "1.12.5")

    testImplementation(group = "org.jetbrains.kotlin", name = "kotlin-test")
    testImplementation(group = "org.jetbrains.kotlin", name = "kotlin-test-junit5")
    testImplementation(group = "io.kotlintest", name = "kotlintest-runner-junit5", version = "3.4.2")
    testImplementation(group = "org.mockito", name = "mockito-core", version = "3.11.2")

    testRuntimeOnly(group = "org.junit.jupiter", name = "junit-jupiter-engine", version = "5.7.2")

}

gradlePlugin {

    val huggsTestConfigurator by plugins.creating {
        id = "us.gov.dot.faa.ang.c55.gradle.huggs.test-configurator"
        displayName = "Gradle Test Task Configurator"
        description = "Gradle Plugin to assist in providing opinionated configuration of test tasks"
        implementationClass = "us.gov.dot.faa.ang.c55.gradle.huggs.testconfigurator.TestConfiguratorPlugin"
    }

}

pluginBundle {
    website = "https://gitlab.com/federal-aviation-administration/ang-c55/gradle/huggs-test-configurator"
    vcsUrl = "https://gitlab.com/federal-aviation-administration/ang-c55/gradle/huggs-test-configurator.git"
    tags = listOf("testing", "junit", "junit 5")
}

tasks.named<Wrapper>("wrapper") {

    gradleVersion = "5.6.4"
    distributionType = Wrapper.DistributionType.ALL

}

tasks.register<org.jetbrains.dokka.gradle.DokkaTask>("dokkaJavadoc") {

    outputFormat = "javadoc"
    outputDirectory = "$buildDir/javadoc"

}

// the inclusion of dokka doc happens after evaluation due to the "publishPluginJavaDocsJar" being auto generated during
// project evaluation. It won't definitely exist until after that point in the build lifecycle.
project.afterEvaluate {

    tasks.named<Jar>("publishPluginJavaDocsJar") {
        from(tasks.named("dokkaJavadoc"))
    }

}

testSets {

    val functionalTest by creating(){}
    val integrationTest by creating(){}

}

tasks.named("check").configure {

    dependsOn("functionalTest")
    dependsOn("integrationTest")

}

tasks.named("integrationTest").configure{

    mustRunAfter("test")

}

tasks.named("functionalTest").configure{

    mustRunAfter("integrationTest")

}

tasks.withType<Test> {

    useJUnitPlatform()
    testLogging.showStandardStreams = true

}

tasks.withType<KotlinCompile> {

    val javaVersion = JavaVersion.VERSION_1_8.toString()

    sourceCompatibility = javaVersion
    targetCompatibility = javaVersion
    kotlinOptions.jvmTarget = javaVersion
    kotlinOptions.freeCompilerArgs = listOf("-Xjvm-default=enable")

}

publishing.publications.withType<MavenPublication> {

    pom {
        organization {
            name.set("US DOT, FAA, Office of NextGen, Modeling and Simulation Branch")
        }
        url.set("https://gitlab.com/federal-aviation-administration/ang-c55/gradle/huggs-test-configurator")
        scm {
            connection.set("scm:git:https://gitlab.com/federal-aviation-administration/ang-c55/gradle/huggs-test-configurator.git")
            developerConnection.set("scm:git:https://gitlab.com/federal-aviation-administration/ang-c55/gradle/huggs-test-configurator.git")
            url.set("https://gitlab.com/federal-aviation-administration/ang-c55/gradle/huggs-test-configurator")
        }

    }

}
