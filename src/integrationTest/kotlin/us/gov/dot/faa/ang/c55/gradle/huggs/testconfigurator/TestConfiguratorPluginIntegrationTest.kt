/*
 * huggs-test-configurator
 *
 * This is a work created by or on behalf of the United States Government. To the
 * extent that this work is not already in the public domain by virtue of
 * 17 USC § 105, the FAA waives copyright and neighboring rights in the work
 * worldwide through the CC0 1.0 Universal Public Domain Dedication (which can be
 * found at https://creativecommons.org/publicdomain/zero/1.0/).
 */

package us.gov.dot.faa.ang.c55.gradle.huggs.testconfigurator

import io.kotlintest.shouldBe
import io.kotlintest.shouldNotBe
import io.kotlintest.specs.BehaviorSpec
import org.gradle.api.internal.tasks.testing.junitplatform.JUnitPlatformTestFramework
import org.gradle.api.plugins.JavaPlugin
import org.gradle.api.tasks.testing.Test
import org.gradle.testfixtures.ProjectBuilder

/**
 * @author US DOT, FAA, Office of NextGen, Modeling and Simulation Branch
 */
class TestConfiguratorPluginIntegrationTest : BehaviorSpec({

    Given("a base project") {

        val project = ProjectBuilder.builder().build()

        When ("applying the plugin") {

            project.pluginManager.apply("us.gov.dot.faa.ang.c55.gradle.huggs.test-configurator")

            Then("the HUGGS Test Configurator plugin is applied to the project") {

                project.plugins.getPlugin(TestConfiguratorPlugin::class.java) shouldNotBe null

            }
            Then("the 'java' plugin is applied to the project") {

                project.plugins.getPlugin(JavaPlugin::class.java) shouldNotBe null

            }
            Then("the JUnit 5 Platform dependencies are added to the test dependency configurations") {

                project.configurations.getByName("testImplementation").dependencies.find {
                    it.group == "org.junit.jupiter" && it.name == "junit-jupiter-api" && it.version == "5.7.2"
                } shouldNotBe null

                project.configurations.getByName("testRuntimeOnly").dependencies.find {
                    it.group == "org.junit.jupiter" && it.name == "junit-jupiter-engine" && it.version == "5.7.2"
                } shouldNotBe null

            }
            Then("all test tasks are configured to use JUnit Platform") {

                //TODO make sure that some test tasks actually exist
                project.tasks.withType(Test::class.java).find{ it.testFramework !is JUnitPlatformTestFramework } shouldBe null

            }
            Then("all test tasks are configured to show standard streams to the JVM console") {

                //TODO make sure that some test tasks actually exist
                project.tasks.withType(Test::class.java).find { !it.testLogging.showStandardStreams } shouldBe null

            }


        }
    }
})
