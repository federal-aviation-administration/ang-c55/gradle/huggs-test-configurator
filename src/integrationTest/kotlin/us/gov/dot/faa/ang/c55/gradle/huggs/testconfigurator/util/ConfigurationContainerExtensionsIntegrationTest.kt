/*
 * huggs-test-configurator
 *
 * This is a work created by or on behalf of the United States Government. To the
 * extent that this work is not already in the public domain by virtue of
 * 17 USC § 105, the FAA waives copyright and neighboring rights in the work
 * worldwide through the CC0 1.0 Universal Public Domain Dedication (which can be
 * found at https://creativecommons.org/publicdomain/zero/1.0/).
 */

package us.gov.dot.faa.ang.c55.gradle.huggs.testconfigurator.util

import io.kotlintest.shouldNotBe
import io.kotlintest.specs.BehaviorSpec
import org.gradle.api.Project
import org.gradle.api.artifacts.ConfigurationContainer
import org.gradle.testfixtures.ProjectBuilder

/**
 * @author US DOT, FAA, Office of NextGen, Modeling and Simulation Branch
 */
internal class ConfigurationContainerExtensionsIntegrationTest: BehaviorSpec({

    Given("a ConfigurationContainer") {

        val project: Project = ProjectBuilder.builder().build()
        val configurationContainer: ConfigurationContainer = project.configurations

        And("existing configurations") {

            configurationContainer.create("testImplementation")
            configurationContainer.create("testRuntimeOnly")

            When("adding a map of dependencies") {

                val projectDependencies = mapOf(
                        "testImplementation" to listOf(project.dependencies.create("dev.test:test-api:1.0")),
                        "testRuntimeOnly" to listOf(project.dependencies.create("dev.test:test-impl:1.0"))
                )

                configurationContainer.addDependencyMap(projectDependencies)

                Then( "each dependency is added to the coordinating configuration") {

                    configurationContainer.getByName("testImplementation").dependencies.find {
                        it.group == "dev.test" && it.name == "test-api" && it.version == "1.0"
                    } shouldNotBe null

                    configurationContainer.getByName("testRuntimeOnly").dependencies.find {
                        it.group == "dev.test" && it.name == "test-impl" && it.version == "1.0"
                    } shouldNotBe null

                }
            }
        }
    }
})
