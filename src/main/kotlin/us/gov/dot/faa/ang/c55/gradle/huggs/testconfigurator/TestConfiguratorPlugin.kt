/*
 * huggs-test-configurator
 *
 * This is a work created by or on behalf of the United States Government. To the
 * extent that this work is not already in the public domain by virtue of
 * 17 USC § 105, the FAA waives copyright and neighboring rights in the work
 * worldwide through the CC0 1.0 Universal Public Domain Dedication (which can be
 * found at https://creativecommons.org/publicdomain/zero/1.0/).
 */
@file:Suppress("UnstableApiUsage")

package us.gov.dot.faa.ang.c55.gradle.huggs.testconfigurator

import org.gradle.api.Project
import org.gradle.api.Plugin
import org.gradle.api.tasks.testing.Test
import us.gov.dot.faa.ang.c55.gradle.huggs.testconfigurator.util.applyAll
import us.gov.dot.faa.ang.c55.gradle.huggs.testconfigurator.util.addDependencyMap

/**
 * A Gradle plugin to assist in providing opinionated/conventional configuration of test tasks.
 *
 * @author US DOT, FAA, Office of NextGen, Modeling and Simulation Branch
 */
class TestConfiguratorPlugin: Plugin<Project> {

    companion object {
        /**
         * The preferred version of JUNIT 5
         */
        const val JUNIT_VERSION = "5.7.2"
    }

    /**
     * Applies the plugin to the provided project.
     *
     * @param theProject the Project to which the plugin should be applied
     */
    override fun apply(theProject: Project) {

        //Map of dependency configuration names, to a list of dependencies to be added to the configuration
        val projectDependencies = mapOf(
                "testImplementation" to listOf(
                        theProject.dependencies.create("org.junit.jupiter:junit-jupiter-api:${JUNIT_VERSION}")),
                "testRuntimeOnly" to listOf(
                        theProject.dependencies.create("org.junit.jupiter:junit-jupiter-engine:${JUNIT_VERSION}"))
        )

        theProject.logger.debug("Applying Gradle JUnit plugin")

        //The java plugin ensures that the dependency configurations that'll be modified will already be in place
        theProject.pluginManager.applyAll("java")
        theProject.configurations.addDependencyMap(projectDependencies)
        theProject.configureTestTasks()

        theProject.logger.debug("Gradle JUnit plugin applied!")

    }

    /**
     * Configures the test tasks of the receiver Project.
     */
    private fun Project.configureTestTasks() {

        logger.debug("Configuring test tasks")

        tasks.withType(Test::class.java).configureEach {
            it.useJUnitPlatform()
            it.testLogging.showStandardStreams = true
        }

        logger.debug("Test task configuration complete!")

    }

}
