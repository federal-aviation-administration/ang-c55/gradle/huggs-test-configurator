/*
 * huggs-test-configurator
 *
 * This is a work created by or on behalf of the United States Government. To the
 * extent that this work is not already in the public domain by virtue of
 * 17 USC § 105, the FAA waives copyright and neighboring rights in the work
 * worldwide through the CC0 1.0 Universal Public Domain Dedication (which can be
 * found at https://creativecommons.org/publicdomain/zero/1.0/).
 */

package us.gov.dot.faa.ang.c55.gradle.huggs.testconfigurator.util

import mu.KotlinLogging
import org.gradle.api.artifacts.ConfigurationContainer
import org.gradle.api.artifacts.Dependency

/**
 * Extension functions for ConfigurationContainer
 *
 * @author US DOT, FAA, Office of NextGen, Modeling and Simulation Branch
 */

private val logger = KotlinLogging.logger {}

/**
 * Adds a map of dependencies to the receiver ConfigurationContainer (with keys of configuration names to a value of
 * a list of dependencies to be applied to the configuration). A configuration with name coordinating to each key must
 * already exist.
 *
 * @param theConfigurationNameToDependenciesMap The map of dependencies to add, keyed to configuration name, and values
 * of a list of dependencies to be added to that configuration.
 */
fun ConfigurationContainer.addDependencyMap(theConfigurationNameToDependenciesMap : Map<String, Collection<Dependency>>) {

    logger.debug{ "Adding dependency map: $theConfigurationNameToDependenciesMap" }

    theConfigurationNameToDependenciesMap.forEach { (dependencyConfigurationName, dependencyList) ->

        this.addAllDependenciesToConfiguration(dependencyConfigurationName, dependencyList)

    }

    logger.debug{ "Dependency addition complete!" }

}

/**
 * Adds a list of dependencies to the specified configuration within the receiver ConfigurationContainer. A
 * configuration with name coordinating to the specified name must already exist.
 *
 * @param theConfigurationName The name of the configuration to which the dependencies should be added.
 * @param theDependencies The list of dependencies to add to the configuration.
 */
fun ConfigurationContainer.addAllDependenciesToConfiguration(theConfigurationName: String, theDependencies: Collection<Dependency>) {

    logger.debug{ "Adding dependencies to configuration. Configuration: $theConfigurationName, Dependencies: $theDependencies" }

    theDependencies.forEach { dependency ->

        this.addDependencyToConfiguration(theConfigurationName, dependency)

    }

    logger.debug{ "All dependencies added to configuration!"}

}

/**
 * Adds a dependency to the specified configuration within the receiver ConfigurationContainer. A configuration with
 * name coordinating to the specified name must already exist.
 *
 * @param theConfigurationName The name of the configuration to which the dependency should be added.
 * @param theDependency The dependency to add to the configuration.
 */
fun ConfigurationContainer.addDependencyToConfiguration(theConfigurationName: String, theDependency: Dependency) {

    logger.debug{ "Adding dependency to configuration. Configuration: $theConfigurationName, Dependency: $theDependency" }

    this.named(theConfigurationName).configure { configuration ->

        configuration.dependencies.add(theDependency)

    }

    logger.debug{ "Dependency added to configuration!"}

}