/*
 * huggs-test-configurator
 *
 * This is a work created by or on behalf of the United States Government. To the
 * extent that this work is not already in the public domain by virtue of
 * 17 USC § 105, the FAA waives copyright and neighboring rights in the work
 * worldwide through the CC0 1.0 Universal Public Domain Dedication (which can be
 * found at https://creativecommons.org/publicdomain/zero/1.0/).
 */

package us.gov.dot.faa.ang.c55.gradle.huggs.testconfigurator.util

import org.gradle.api.plugins.PluginManager
import mu.KotlinLogging

/**
 * Extension functions for PluginManager
 *
 * @author US DOT, FAA, Office of NextGen, Modeling and Simulation Branch
 */

private val logger = KotlinLogging.logger {}

/**
 * Applies plugins with the specified ids to the receiver Project
 *
 * @param thePluginIds the IDs of the plugins to be applied
 */
internal fun PluginManager.applyAll(vararg thePluginIds: String) {

    logger.debug {"Applying plugins: $thePluginIds"}

    thePluginIds.forEach { pluginId ->

        this.apply(pluginId)

    }

    logger.debug {"Plugins applied!"}

}
