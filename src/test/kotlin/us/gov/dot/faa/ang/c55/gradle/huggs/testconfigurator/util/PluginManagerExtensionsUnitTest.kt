/*
 * huggs-test-configurator
 *
 * This is a work created by or on behalf of the United States Government. To the
 * extent that this work is not already in the public domain by virtue of
 * 17 USC § 105, the FAA waives copyright and neighboring rights in the work
 * worldwide through the CC0 1.0 Universal Public Domain Dedication (which can be
 * found at https://creativecommons.org/publicdomain/zero/1.0/).
 */

package us.gov.dot.faa.ang.c55.gradle.huggs.testconfigurator.util

import io.kotlintest.DisplayName
import org.gradle.api.plugins.PluginManager
import org.junit.jupiter.api.Nested
import org.junit.jupiter.api.Tag
import org.junit.jupiter.api.Test
import org.mockito.ArgumentMatchers.any
import org.mockito.BDDMockito.then
import org.mockito.Mockito
import org.mockito.Mockito.never
import org.mockito.Mockito.times

/**
 * @author US DOT, FAA, Office of NextGen, Modeling and Simulation Branch
 */
@Tag("unit")
internal class PluginManagerExtensionsUnitTest {

    @Nested
    @DisplayName("applyAll")
    inner class ApplyAllTests {

        @Nested
        @DisplayName("Given a PluginManager,")
        inner class GivenAPluginManager {

            private val mockPluginManager = Mockito.mock(PluginManager::class.java)!!

            @Test
            fun `when applying a single plugin, then only a single plugin is applied`() {

                mockPluginManager.applyAll("java")

                then(mockPluginManager).should().apply("java")

            }

            @Test
            fun `when applying multiple plugins, then all of the plugins are applied`() {

                val pluginsIdsToApply = arrayOf("java", "maven-publish", "idea")

                mockPluginManager.applyAll(*pluginsIdsToApply)

                //Verify that apply was only called 3 times and that all the elements on the expected list were applied
                then(mockPluginManager).should(times(3)).apply(any(String::class.java))
                pluginsIdsToApply.forEach {pluginId ->
                    then(mockPluginManager).should().apply(pluginId)
                }

            }

            @Test
            fun `when applying an empty list of plugins, then no plugins are applied`() {

                mockPluginManager.applyAll()

                then(mockPluginManager).should(never()).apply(any(String::class.java))

            }
        }
    }
}
